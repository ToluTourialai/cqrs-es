<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180111131101 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Post ADD class VARCHAR(255)');
        $this->addSql('ALTER TABLE Post ADD uuid VARCHAR(255)');
        $this->addSql('ALTER TABLE Author ADD class VARCHAR(255)');

    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Post DROP class');
        $this->addSql('ALTER TABLE Post DROP uuid');
        $this->addSql('ALTER TABLE Author DROP class');

    }
}
