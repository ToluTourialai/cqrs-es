<?php

namespace Tests\BlogBundle\Mail;

use Blog\Mail\Event\EmailWasSendRegardingBlogPost;

class EmailWasSendRegardingBlogPostTest extends \PHPUnit_Framework_TestCase
{
    public function testDeserializeCorrectObject()
    {
        $result = EmailWasSendRegardingBlogPost::deserialize([]);

        $this->assertInstanceOf(EmailWasSendRegardingBlogPost::class, $result);
    }

    public function testSerializeReturnsArray()
    {
        $event = new EmailWasSendRegardingBlogPost();

        $result = $event->serialize();

        $this->assertSame([], $result);
    }
}