<?php

namespace Tests\Blog\Author;

use Blog\Author\Author;
use Blog\Author\Command\CreateAuthor;
use Blog\Author\Command\EditAuthor;
use Blog\Author\AuthorCommandHandler;
use Blog\Author\Event\AuthorWasCreated;
use Blog\Author\Event\AuthorWasEdited;
use Broadway\CommandHandling\CommandHandler;
use Broadway\CommandHandling\Testing\CommandHandlerScenarioTestCase;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

class AuthorDomainTest extends CommandHandlerScenarioTestCase
{
    const ID = '3e42b23a-bc7b-4e32-8a5b-c5d38eb9906f';
    const AUTHOR_ID = 1;
    const AUTHOR_F_NAME = 'Tolu';
    const AUTHOR_L_NAME = 'Tourialai';
    const AUTHOR_F_NAME_EDIT = 'Eric';
    const AUTHOR_L_NAME_EDIT = 'Schildkamp';
    const AUTHOR_M_NAME = '';
    const AUTHOR_MAIL = 'tolu@alphacomm.nl';
    const AUTHOR_MAIL_EDIT = 'eric@alphacomm.nl';

    /**
     * @test
     */
    public function newAuthorWasAdded()
    {
        $id = self::ID;
        $authorId = self::AUTHOR_ID;
        $authorFirstName = self::AUTHOR_F_NAME;
        $authorLastName = self::AUTHOR_L_NAME;
        $authorMiddleName = self::AUTHOR_M_NAME;
        $authorMail = self::AUTHOR_MAIL;

        $command = new CreateAuthor($id, $authorId, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail);
        $event = new AuthorWasCreated($id, $authorId, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail);

        $this->createScenario()
            ->given([])
            ->when(
                $command
            )
            ->then([
                $event
            ]);
    }

    /**
     * @test
     */
    public function existingAuthorWasEdited()
    {
        $id = self::ID;
        $authorId = self::AUTHOR_ID;
        $authorFirstName = self::AUTHOR_F_NAME;
        $authorFirstNameEdit = self::AUTHOR_F_NAME_EDIT;
        $authorLastName = self::AUTHOR_L_NAME;
        $authorLastNameEdit = self::AUTHOR_L_NAME_EDIT;
        $authorMiddleName = self::AUTHOR_M_NAME;
        $authorMail = self::AUTHOR_MAIL;
        $authorMailEdit = self::AUTHOR_MAIL_EDIT;

        $givenEvent = new AuthorWasCreated($id, $authorId, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail);
        $command = new EditAuthor($id, $authorFirstNameEdit, $authorLastNameEdit, $authorMiddleName, $authorMailEdit);
        $event = new AuthorWasEdited($authorFirstNameEdit, $authorLastNameEdit, $authorMiddleName, $authorMailEdit);

        $this->createScenario()
            ->withAggregateId($id)
            ->given([
                $givenEvent
            ])
            ->when(
                $command
            )
            ->then([
                $event
            ]);
    }

    /**
     * Create a command handler for the given scenario test case.
     *
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     *
     * @return CommandHandler
     */
    protected function createCommandHandler(EventStore $eventStore, EventBus $eventBus): CommandHandler
    {
        $repository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Author::class,
            new PublicConstructorAggregateFactory()
        );

        $handler = new AuthorCommandHandler(
            $repository
        );

        return $handler;
    }
}