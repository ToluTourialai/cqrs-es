<?php

namespace Tests\Blog\ReadModel;

use Blog\Author\Event\AuthorWasCreated;
use Blog\Author\Event\AuthorWasEdited;
use Blog\ReadModel\Author;
use Blog\ReadModel\AuthorProjector;
use Broadway\ReadModel\InMemory\InMemoryRepository;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Testing\ProjectorScenarioTestCase;

class GetAllUsersProjectorTest extends ProjectorScenarioTestCase
{

    const ID = '3e42b23a-bc7b-4e32-8a5b-c5d38eb9906f';
    const AUTHOR_ID = 1;
    const AUTHOR_F_NAME = 'Tolu';
    const AUTHOR_F_NAME_EDIT = 'Eric';
    const AUTHOR_L_NAME = 'Tourialai';
    const AUTHOR_L_NAME_EDIT = 'Schildkamp';
    const AUTHOR_M_NAME = '';
    const AUTHOR_MAIL = 'tolu@alphacomm.nl';
    const AUTHOR_MAIL_EDIT = 'eric@alphacomm.nl';

    /**
     * @test
     */
    public function authorCreated()
    {
        $id = self::ID;
        $authorId = self::AUTHOR_ID;
        $authorFirstName = self::AUTHOR_F_NAME;
        $authorLastName = self::AUTHOR_L_NAME;
        $authorMiddleName = self::AUTHOR_M_NAME;
        $authorMail = self::AUTHOR_MAIL;

        $command = new AuthorWasCreated($id, $authorId,$authorFirstName, $authorLastName, $authorMiddleName, $authorMail);
        $expected = Author::create($id, $authorId,$authorFirstName, $authorLastName, $authorMiddleName, $authorMail);

        $this->createScenario()
            ->given([])
            ->when(
                $command
            )
            ->then([
                $expected
            ]);
    }

    /**
     * @test
     */
    public function authorEdited()
    {
        $id = self::ID;
        $authorId = self::AUTHOR_ID;
        $authorFirstName = self::AUTHOR_F_NAME;
        $authorFirstNameEdit = self::AUTHOR_F_NAME_EDIT;
        $authorLastName = self::AUTHOR_L_NAME;
        $authorLastNameEdit = self::AUTHOR_L_NAME_EDIT;
        $authorMiddleName = self::AUTHOR_M_NAME;
        $authorMail = self::AUTHOR_MAIL;
        $authorMailEdit = self::AUTHOR_MAIL_EDIT;
        $editAuthor = new Author();

        $firstEvent = new AuthorWasCreated($id, $authorId,$authorFirstName, $authorLastName, $authorMiddleName, $authorMail);
        $command = new AuthorWasEdited($authorFirstNameEdit, $authorLastNameEdit, $authorMiddleName, $authorMailEdit);
        $expected = $editAuthor->edit($id, $authorFirstNameEdit, $authorLastNameEdit, $authorMiddleName, $authorMailEdit);

        $this->createScenario()
            ->withAggregateId($id)
            ->given([])
            ->when($firstEvent)
            ->when($command)
            ->then([
                $expected
            ]);
    }

    /**
     * @param InMemoryRepository $repository
     * @return Projector
     */
    protected function createProjector(InMemoryRepository $repository): Projector
    {
        return new AuthorProjector($repository);
    }
}