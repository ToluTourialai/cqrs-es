<?php

namespace Tests\Blog\ReadModel;

use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Event\BlogPostWasEdited;
use Blog\ReadModel\BlogPost;
use Blog\ReadModel\SingleBlogPostProjector;
use Broadway\ReadModel\InMemory\InMemoryRepository;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Testing\ProjectorScenarioTestCase;
use Broadway\Domain\DateTime;

class SingleBlogPostProjectorTest extends ProjectorScenarioTestCase
{
    const ID = '0';
    const TITLE = 'Dit is een titel';
    const AUTHOR_ID = 1;
    const EDITED_AUTHOR_ID = 2;
    const CONTENT = 'Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content.';
    const EDITED_CONTENT = 'Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content. Dit is aangepaste content.';

    /**
     * @test
     */
    public function blogPostIsCreated()
    {
        $id = self::ID;
        $uuid = 0;
        $title = self::TITLE;
        $authorId = self::AUTHOR_ID;
        $content = self::CONTENT;

        $command = new BlogPostWasCreated($id, $id, $title, $authorId, $content);
        $createdOn= DateTime::fromString('2016-01-01 13:33:37');
        $expected = BlogPost::create($id, $uuid, $title, $authorId, $content, $createdOn);

        $this->createScenario()
            ->given([])
            ->when(
                $command, $createdOn
            )
            ->then([
                $expected,
            ]);
    }

    /**
     * @test
     */
    public function blogPostIsEdited()
    {
        $id = self::ID;
        $uuid = 0;
        $title = self::TITLE;
        $authorId = self::AUTHOR_ID;
        $content = self::CONTENT;
        $editedAuthor = self::EDITED_AUTHOR_ID;
        $editedContent = self::EDITED_CONTENT;

        $blogPostWasCreated = new BlogPostWasCreated($id, $id, $title, $authorId, $content);
        $testEvent = new BlogPostWasEdited($title, $editedAuthor, $editedContent);
        $editedOn = DateTime::fromString('2018-01-02 13:33:37');
        $createdOn = DateTime::fromString('2018-01-01 13:33:37');

        $blogPost = new BlogPost();

        $this->createScenario()
            ->withAggregateId($id)
            ->withDateTimeGenerator(function ($event) {
                if ($event instanceof BlogPostWasCreated) {
                    return DateTime::fromString('2018-01-01 13:33:37');
                }
            })
            ->given([
                $blogPostWasCreated
            ])
            ->when($testEvent, $editedOn)
            ->then([
                $blogPost::deserialize([
                    'id' => $id,
                    'uuid' => $uuid,
                    'title' => $title,
                    'author_id' => $editedAuthor,
                    'content' => $editedContent,
                    'created_on' => $createdOn->toString(),
                    'edited_on' => $editedOn->toString(),
                ])
            ]);
    }

    /**
     * @param InMemoryRepository $repository
     * @return Projector
     */
    protected function createProjector(InMemoryRepository $repository): Projector
    {
        return new SingleBlogPostProjector($repository);
    }
}