<?php

namespace Tests\Blog\ReadModel;

use Blog\Blog\Event\BlogPostWasCreated;
use Blog\ReadModel\MultipleBlogPostProjector;
use Blog\ReadModel\OverView;
use Broadway\Domain\DateTime;
use Broadway\ReadModel\InMemory\InMemoryRepository;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Testing\ProjectorScenarioTestCase;
use Ramsey\Uuid\Uuid;

class MultipleBlogPostProjectorTest extends ProjectorScenarioTestCase
{
    const TITLE = 'Dit is een title';
    const CONTENT = 'Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content.';

    /**
     * @test
     */
    public function getBlogPostOverview()
    {
        $overview = new OverView();
        $title = self::TITLE;
        $content = self::CONTENT;

        $firstUuid = Uuid::uuid4();
        $firstAuthorId = 1;
        $dateCreatedFirst = DateTime::fromString('2018-01-01 13:33:37');
        $firstPostEventListener = new BlogPostWasCreated($firstUuid,$firstUuid,$title,$firstAuthorId, $content);
        $overview->addPost($firstUuid,$title,$firstAuthorId, $content, $dateCreatedFirst);

        $thirdUuid = Uuid::uuid4();
        $thirdAuthorId = 2;
        $dateCreatedThird = DateTime::fromString('2018-01-03 13:33:37');
        $thirdPostEventListener = new BlogPostWasCreated($thirdUuid,$thirdUuid,$title,$thirdAuthorId, $content);
        $overview->addPost($thirdUuid,$title,$thirdAuthorId, $content, $dateCreatedThird);

        $lastId = Uuid::uuid4();
        $event = new BlogPostWasCreated($lastId, $lastId, $title, 4, $content);
        $currentDate = DateTime::now();

        $overview->addPost($lastId, $title, 4, $content, $currentDate);


        $this->createScenario()
            ->withAggregateId(1)
            ->given([
            ])
            ->when($firstPostEventListener, $dateCreatedFirst)
            ->when($thirdPostEventListener, $dateCreatedThird)
            ->when($event, $currentDate)
            ->then([
                $overview
            ]);
    }

    /**
     * @param InMemoryRepository $repository
     * @return Projector
     */
    protected function createProjector(InMemoryRepository $repository): Projector
    {
        return new MultipleBlogPostProjector($repository);
    }
}