<?php

namespace Tests\Blog\BlogPost;

use Blog\Blog\BlogPost;
use Blog\Blog\BlogPostCommandHandler;
use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Command\CreateBlogPostCommand;
use Blog\Blog\Command\EditBlogPostCommand;
use Blog\Blog\Event\BlogPostWasEdited;
use Broadway\CommandHandling\CommandHandler;
use Broadway\CommandHandling\Testing\CommandHandlerScenarioTestCase;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventHandling\EventBus;
use Broadway\EventStore\EventStore;

class BlogPostDomainTest extends CommandHandlerScenarioTestCase
{
    const UUID = '3e42b23a-bc7b-4e32-8a5b-c5d38eb9906f';
    const ID = '0';
    const TITLE = 'Dit is een titel';
    const AUTHOR_ID = 1;
    const CONTENT = 'Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content.';
    const EDITED_AUTHOR_ID = 2;
    const EDITED_CONTENT = 'Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content.';

    /**
     * @test
     */
    public function blogPostWasCreated()
    {
        $id = self::ID;
        $title = self::TITLE;
        $author = self::AUTHOR_ID;
        $content = self::CONTENT;

        $this->createScenario()
            ->given([])
            ->when(
                new CreateBlogPostCommand($id, $id, $title, $author, $content)
            )
            ->then([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ]);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage Post already exists.
     */
    public function throwsErrorWhenPostWithSameIdIsCreated()
    {
        $id = self::ID;
        $title = self::TITLE;
        $author = self::AUTHOR_ID;
        $content = self::CONTENT;

        $this->createScenario()
            ->withAggregateId($id)
            ->given([
                new BlogPostWasCreated($id, $id, $title, $author, $content),
            ])
            ->when(
                new CreateBlogPostCommand($id, $id, $title, $author, $content)
            )
            ->then([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ]);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage Must identify yourself. Select your name.
     */
    public function throwsErrorWhenNoAuthorSpecified()
    {
        $id = self::ID;
        $title = self::TITLE;
        $author = null;
        $content = self::CONTENT;

        $this->createScenario()
            ->given([])
            ->when(
                new CreateBlogPostCommand($id, $id, $title, $author, $content)
            )
            ->then([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ]);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage Title cannot be empty.
     */
    public function throwsErrorWhenEmptyTitle()
    {
        $id = self::ID;
        $title = null;
        $author = self::AUTHOR_ID;
        $content = self::CONTENT;

        $this->createScenario()
            ->given([])
            ->when(
                new CreateBlogPostCommand($id, $id, $title, $author, $content)
            )
            ->then([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ]);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage Content cannot be empty.
     */
    public function throwsErrorWhenEmptyContent()
    {
        $id = self::ID;
        $title = self::TITLE;
        $author = self::AUTHOR_ID;
        $content = null;

        $this->createScenario()
            ->given([])
            ->when(
                new CreateBlogPostCommand($id, $id, $title, $author, $content)
            )
            ->then([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ]);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage Title and/or content is too short.
     */
    public function throwsErrorWhenContentAndTitleAreTooShort()
    {
        $id = self::ID;
        $title = substr(self::TITLE, 0, 4);
        $author = self::AUTHOR_ID;
        $content = substr(self::CONTENT,0,4);

        $this->createScenario()
            ->given([])
            ->when(
                new CreateBlogPostCommand($id, $id, $title, $author, $content)
            )
            ->then([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ]);
    }

    /**
     * @test
     */
    public function existingBlogPostWasEdited()
    {
        $id = self::ID;
        $title = self::TITLE;
        $author = self::AUTHOR_ID;
        $content = self::CONTENT;
        $editedAuthor = self::EDITED_AUTHOR_ID;
        $editedContent = self::EDITED_CONTENT;

        $this->createScenario()
            ->withAggregateId($id)
            ->given([
                new BlogPostWasCreated($id, $id, $title, $author, $content)
            ])
            ->when(
                new EditBlogPostCommand($id, $title, $editedAuthor, $editedContent)
            )
            ->then([
                new BlogPostWasEdited($title, $editedAuthor, $editedContent)
            ]);
    }

    /**
     * Create a command handler for the given scenario test case.
     *
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     *
     * @return CommandHandler
     */
    protected function createCommandHandler(EventStore $eventStore, EventBus $eventBus): CommandHandler
    {
        $repository = new EventSourcingRepository(
                    $eventStore,
                    $eventBus,
                    BlogPost::class,
                    new PublicConstructorAggregateFactory()
            );

        $handler = new BlogPostCommandHandler(
            $repository
        );

        return $handler;
    }
}