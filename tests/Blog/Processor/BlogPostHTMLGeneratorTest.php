<?php

namespace Tests\Blog\Processor;

use Blog\Blog\BlogPost;
use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Event\BlogPostWasEdited;
use Blog\Blog\Processor\BlogPostHTMLGenerator;
use Broadway\Domain\DateTime;
use Broadway\Domain\DomainMessage;
use Broadway\Domain\Metadata;
use Broadway\ReadModel\Repository;
use Symfony\Component\Filesystem\Filesystem;

class BlogPostHTMLGeneratorTest extends \PHPUnit_Framework_TestCase
{
    const ID = '0';
    const UUID = '3e42b23a-bc7b-4e32-8a5b-c5d38eb9906f';
    const TITLE = 'Dit is een titel';
    const AUTHOR_ID = 1;
    const CONTENT = 'Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content.';
    const EDITED_AUTHOR_ID = 2;
    const EDITED_CONTENT = 'Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content.';

    /** @var Filesystem */
    private $fs;

    /** @var Repository */
    private $readModelRepository;

    /** @var BlogPostHTMLGenerator */
    private $processor;

    public function setUp()
    {
        $this->fs = $this->prophesize(Filesystem::class);
        $this->readModelRepository = $this->prophesize(Repository::class);

        $this->processor = new BlogPostHTMLGenerator($this->fs->reveal(), $this->readModelRepository->reveal());
    }

    /**
     * @test
     */
    public function testHandleBlogPostWasCreated()
    {
        $this->processor->handle(
            new DomainMessage(
                self::ID,
                1,
                new Metadata(),
                new BlogPostWasCreated(
                    self::ID,
                    self::ID,
                    self::TITLE,
                    self::AUTHOR_ID,
                    self::CONTENT
                ),
                DateTime::now()
            )
        );
    }

    /**
     * @test
     */
    public function testHandleBlogPostWasEdited()
    {
        $this->processor->handle(
            new DomainMessage(
                self::ID,
                2,
                new Metadata(),
                new BlogPostWasEdited(
                    self::TITLE,
                    self::EDITED_AUTHOR_ID,
                    self::EDITED_CONTENT
                ),
                DateTime::now()
            )
        );
    }
}