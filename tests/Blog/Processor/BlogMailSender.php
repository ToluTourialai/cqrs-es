<?php

namespace Tests\Blog\Processor;

use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Processor\BlogMailSender;
use Blog\PostRepository;
use Broadway\Domain\DateTime;
use Broadway\Domain\DomainMessage;
use Blog\AuthorRepository;
use Broadway\Domain\Metadata;
use Broadway\ReadModel\InMemory\InMemoryRepository;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Testing\ProjectorScenarioTestCase;
use Swift_Mailer as Mailer;

class BlogMailSender extends ProjectorScenarioTestCase
{
    const ID = '3e42b23a-bc7b-4e32-8a5b-c5d38eb9906f';
    const TITLE = 'Dit is een titel';
    const CONTENT = 'Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content. Dit is content.';
    const EDITED_CONTENT = 'Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content.';

    const AUTHOR_UUID = '3e42b23a-bc7b-4e32-8a5b-c5d38eb99sef';
    const AUTHOR_NAME = 'Tolu';
    const AUTHOR_LAST_NAME = 'Tourialai';
    const AUTHOR_MAIL = 'tolu@alphacomm.nl';
    const AUTHOR_ID = 1;
    const EDITED_AUTHOR_ID = 2;

    /** @var Mailer */
    private $mailer;

    /** @var AuthorRepository */
    private $authorRepository;

    /** @var PostRepository */
    private $postRepository;

    /** @var BlogMailSender */
    private $processor;

    public function setUp()
    {
        $this->mailer = $this->prophesize(Mailer::class);
        $this->authorRepository = $this->prophesize(AuthorRepository::class);
        $this->authorRepository->makeProphecyMethodCall('findByAuthorById')->return;

        $this->postRepository = $this->prophesize(PostRepository::class);

        $this->processor = new BlogMailSender($this->mailer->reveal(), $this->postRepository->reveal(), $this->authorRepository->reveal());
    }

    public function testHandleBlogPostWasCreated()
    {
        $this->processor->handle(
            new DomainMessage(
                self::ID,
                0,
                new Metadata(),
                new BlogPostWasCreated(
                    self::ID,
                    self::TITLE,
                    self::AUTHOR_ID,
                    self::CONTENT
                ),
                DateTime::now()
            )
        );
    }

    /**
     * @return Projector
     */
    protected function createProjector(InMemoryRepository $repository)
    {
        // TODO: Implement createProjector() method.
    }
}