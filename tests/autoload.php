<?php

date_default_timezone_set('UTC');

$loader = require dirname(__DIR__).'/app/autoload.php';
$loader->addPsr4('', __DIR__);

return $loader;


