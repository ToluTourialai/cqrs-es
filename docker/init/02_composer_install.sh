#!/bin/bash
cd /site

export COMPOSER_HOME=/.composer
mkdir $COMPOSER_HOME
cp /config/parameters.yml /site/app/config/parameters.yml
composer install --no-interaction --no-progress
rm -rf $COMPOSER_HOME

chown -R www-data:www-data /var/app