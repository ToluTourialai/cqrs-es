#!/bin/bash

echo Create geoip var directory
mkdir /var/geoip
chown -R www-data:www-data /var/geoip
chmod 755 -R /var/geoip

echo Downloading geoip country database
wget -N http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz -P /tmp/
gunzip /tmp/GeoIP.dat.gz
mv /tmp/GeoIP.dat /var/geoip/.