FROM phusion/baseimage:0.9.20

MAINTAINER reen@alphacomm.nl

ENV DEBIAN_FRONTEND noninteractive

RUN add-apt-repository -y ppa:ondrej/php && \
    add-apt-repository -y ppa:nginx/stable && \
    apt-get update && \
    apt-get install -yq \
    nginx \
    nginx-extras \
    wget \
    curl \
    git \
    php7.1-cli \
    php7.1-fpm \
    php7.1-mysql \
    php7.1-sqlite \
    php7.1-curl \
    php7.1-gd \
    php7.1-mcrypt \
    php7.1-memcached \
    php7.1-zmq \
    php7.1-soap \
    php7.1-dom \
    php7.1-mbstring \
    php7.1-bcmath \
    php7.1-zip \
    php-xdebug \
    ssmtp \
    mailutils \
    && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/* && \
    curl -sS https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer | php --  --install-dir=/usr/local/bin --filename=composer

ENV SYMFONY_VAR_DIR=/var/app \
    SYMFONY_ENV=dev

RUN echo "sendmail_path=sendmail -i -t" >> /etc/php/7.1/cli/conf.d/10-sendmail.ini && \
    echo "sendmail_path=sendmail -i -t" >> /etc/php/7.1/fpm/conf.d/10-sendmail.ini

ADD docker/config/php-xdebug.ini /etc/php/7.1/mods-available/xdebug.ini

ADD docker/config/ /config/
ADD docker/init/ /etc/my_init.d/
ADD docker/service/ /etc/service/

RUN mkdir -p /var/sessions /var/app /site && \
    chown -R www-data:www-data /var/sessions /var/app /site && \
    chmod +x /etc/service/*/run && \
    chmod +x /etc/my_init.d/*.sh

WORKDIR /site

EXPOSE 80