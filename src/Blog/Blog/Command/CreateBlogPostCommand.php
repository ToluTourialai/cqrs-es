<?php

namespace Blog\Blog\Command;

use Broadway\Serializer\Serializable;

class CreateBlogPostCommand implements Serializable
{
    /** @var string */
    private $id, $uuid, $title, $author, $content;

    /**
     * @param $id
     * @param $title
     * @param $author
     * @param $content
     */
    public function __construct($id, $uuid, $title, $author, $content)
    {
        $this->id = $id;
        $this->uuid = $uuid;
        $this->title = $title;
        $this->author = $author;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    public static function deserialize(array $data): self
    {
        $id = $data['id'];
        $uuid = $data['uuid'];
        $title = $data['title'];
        $author = $data['author'];
        $content = $data['content'];
        unset($data['id']);
        unset($data['uuid']);
        unset($data['title']);
        unset($data['author']);
        unset($data['content']);

        return new self($id, $uuid, $title, $content, $author);
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'title' => $this->title,
            'author' => $this->author,
            'content' => $this->content

        ];
    }
}