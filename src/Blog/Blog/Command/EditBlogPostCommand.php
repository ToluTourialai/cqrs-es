<?php

namespace Blog\Blog\Command;

use Broadway\Serializer\Serializable;

class EditBlogPostCommand implements Serializable
{
    /**
     * @var string
     */
    private $id, $title, $author, $content;

    /**
     * @param $id
     * @param $title
     * @param $author
     * @param $content
     */
    public function __construct($id, $title, $author, $content)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param array
     *
     * @return EditBlogPostCommand
     */
    public static function deserialize(array $data): self
    {
        $id = $data['id'];
        $title = $data['title'];
        $author = $data['author'];
        $content = $data['content'];
        unset($data['id']);
        unset($data['title']);
        unset($data['author']);
        unset($data['content']);

        return new self($id, $title, $author, $content);
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author,
            'content' => $this->content
        ];
    }
}