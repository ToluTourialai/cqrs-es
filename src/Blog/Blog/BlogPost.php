<?php

namespace Blog\Blog;

use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Event\BlogPostWasEdited;
use Broadway\EventSourcing\EventSourcedAggregateRoot;

class BlogPost extends EventSourcedAggregateRoot
{
    /**
     * @var string
     */
    private  $id, $title, $content, $author, $uuid;

    public static function create(
        string $id,
        string $uuid,
        string $title,
        int $author,
        string  $content
    ) {
        $blogPost = new self();

        $blogPost->apply(new BlogPostWasCreated(
            $id,
            $uuid,
            $title,
            $author,
            $content
        ));

        return $blogPost;
    }

    protected function applyBlogPostWasCreated(BlogPostWasCreated $event)
    {
        $this->id = $event->getId();
        $this->uuid = $event->getUuid();
        $this->title = $event->getTitle();
        $this->author = $event->getAuthor();
        $this->content = $event->getContent();
    }

    public function edit(
        string $title,
        int $author,
        string  $content
    ):void
    {
        $this->apply(new BlogPostWasEdited(
            $title,
            $author,
            $content
        ));
    }

    protected function applyBlogPostWasEdited(BlogPostWasEdited $event)
    {
        $this->title = $event->getTitle();
        $this->author = $event->getAuthor();
        $this->content = $event->getContent();
    }

    /**
     * @return string
     */
    public function getAggregateRootId(): string
    {
        return $this->id;
    }
}