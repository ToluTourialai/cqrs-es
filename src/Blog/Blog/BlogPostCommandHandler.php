<?php

namespace Blog\Blog;

use Blog\Blog\Command\CreateBlogPostCommand;
use Blog\Blog\Command\EditBlogPostCommand;
use Broadway\CommandHandling\SimpleCommandHandler;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\Repository\AggregateNotFoundException;
use Symfony\Component\Config\Definition\Exception\Exception;

class BlogPostCommandHandler extends SimpleCommandHandler
{
    /**
     * @var EventSourcingRepository
     */
    private $repository;

    public function __construct(EventSourcingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handleCreateBlogPostCommand(CreateBlogPostCommand $command)
    {
        $id = $command->getId();
        $uuid = $command->getUuid();
        $title = $command->getTitle();
        $content = $command->getContent();

        if ($this->exists($id)) {
            throw new Exception('Post already exists.');
        }

        if (empty($title)) {
            throw new Exception('Title cannot be empty.');
        }

        if (empty($content)) {
            throw new Exception('Content cannot be empty.');
        }

        if (strlen($title) < 5 || strlen($content) < 5 ) {
            throw new Exception('Title and/or content is too short.');
        }

        if (!is_int($command->getAuthor())) {
            throw new Exception('Must identify yourself. Select your name.');
        }

        $blogPost = BlogPost::create(
            $id,
            $uuid,
            $title,
            $command->getAuthor(),
            $content
        );

        $this->repository->save($blogPost);
    }

    public function handleEditBlogPostCommand(EditBlogPostCommand $command):  void
    {
        $id = $command->getId();
        if (!$this->exists($id)) {
            throw new Exception('Post does not exist');
        }

        $originalPost = $this->getBlogPost($id);

        $originalPost->edit(
            $command->getTitle(),
            $command->getAuthor(),
            $command->getContent()
        );

        $this->repository->save($originalPost);
    }

    private function getBlogPost($id): BlogPost
    {
        /** @var BlogPost $post */
        $post = $this->repository->load($id);

        return $post;
    }

    private function exists(string $id): bool
    {
        try {
            $this->getBlogPost($id);

            return true;
        } catch (AggregateNotFoundException $ex) {
            return false;
        }
    }
}