<?php

namespace Blog\Blog\Event;

use Broadway\Serializer\Serializable;

class BlogPostWasCreated implements Serializable
{
    /** @var string */
    private  $id, $author, $content, $title, $uuid;

    public function __construct(
        string $id,
        string $uuid,
        string $title,
        int $author,
        string $content
    ) {
        $this->id = $id;
        $this->uuid = $uuid;
        $this->title = $title;
        $this->author = $author;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAuthor(): int
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return mixed The object instance
     */
    public static function deserialize(array $data)
    {
        $id = $data['id'];
        $uuid = $data['uuid'];
        $title = $data['title'];
        $author = $data['author'];
        $content = $data['content'];

        return new self($id, $uuid, $title, $author, $content);
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'title' => $this->title,
            'author' => $this->author,
            'content' => $this->content,
        ];
    }
}