<?php

namespace Blog\Blog\Event;

use Broadway\Serializer\Serializable;

class BlogPostWasEdited implements Serializable
{
    /** @var string */
    private  $author, $content, $title;

    public function __construct(
        string $title,
        int $author,
        string $content
    ) {
        $this->title = $title;
        $this->author = $author;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getAuthor(): int
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return mixed The object instance
     */
    public static function deserialize(array $data)
    {
        $title = $data['title'];
        $author = $data['author'];
        $content = $data['content'];

        return new self($title, $author, $content);
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'title' => $this->title,
            'author' => $this->author,
            'content' => $this->content,
        ];
    }
}