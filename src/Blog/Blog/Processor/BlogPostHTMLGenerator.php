<?php

namespace Blog\Blog\Processor;

use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Event\BlogPostWasEdited;
use Broadway\Domain\DomainMessage;
use Broadway\Processor\Processor;
use Broadway\ReadModel\Repository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use DateTime;

class BlogPostHTMLGenerator extends Processor
{
    /** @var Filesystem */
    private $fs;

    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    protected function handleBlogPostWasCreated(BlogPostWasCreated $event, DomainMessage $domainMessage)
    {
        $file = '/site/src/BlogBundle/Resources/views/baseHTMLViews/single.html.php';

        if (!file_exists($file)) {
            throw new Exception('Template file not found :'.$file);
        }

        $date = $domainMessage->getRecordedOn()->toString();
        $currentDate = DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $date)->format('d-m-Y H:i:s');

        $templateValues = [
            'id' => $event->getId(),
            'blogTitle' => $event->getTitle(),
            'authorName' =>  $event->getAuthor(),
            'type' => 'Written',
            'date' => $currentDate,
            'edited_on' => '',
            'content' =>  $event->getContent()
        ];

        $output = file_get_contents($file);

        foreach ($templateValues as $key => $value) {
            $replace = "[@$key]";
            $output = str_replace($replace, $value, $output);
        }

        $this->createDirAndFile($event->getTitle(), $output);

    }


    public function handleBlogPostWasEdited(BlogPostWasEdited $event, DomainMessage $domainMessage)
    {
        $file = '/site/src/BlogBundle/Resources/views/baseHTMLViews/single.html.php';

        if (!file_exists($file)) {
            return 'Template file not found';
        }

        $date = $domainMessage->getRecordedOn()->toString();
        $currentDate = DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $date)->format('d-m-Y H:i:s');

        $templateValues = [
            'blogTitle' => $event->getTitle(),
            'authorName' =>  $event->getAuthor(),
            'type' => 'Modified',
            'date' => $currentDate,
            'content' =>  $event->getContent()
        ];

        $output = file_get_contents($file);

        foreach ($templateValues as $key => $value) {
            $replace = "[@$key]";
            $output = str_replace($replace, $value, $output);
        }

        $this->createDirAndFile($event->getTitle(), $output);
    }

    public function createDirAndFile(string $fileName, string $templateContent)
    {
        $fileName = str_replace(' ', '-',strtolower($fileName)).'.html.php';
        $fileDir = '/site/src/BlogBundle/Resources/views/renderedHTMLViews/';

        if (!$this->fs->exists($fileDir)) {
            $this->fs->mkdir($fileDir);
        }

        if (!$this->fs->exists($fileDir.$fileName)) {
            $this->fs->touch($fileDir . $fileName);
        }

        $this->fs->dumpFile($fileDir . $fileName, $templateContent);
    }
}
