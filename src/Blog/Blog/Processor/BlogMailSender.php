<?php

namespace Blog\Blog\Processor;

use Blog\ReadModel\Author;
use Blog\ReadModel\AuthorRepository;
use Blog\Blog\Event\BlogPostWasCreated;
use Broadway\Domain\DomainMessage;
use Swift_Mailer as Mailer;
use Swift_Message as Message;

use Broadway\Processor\Processor;

class BlogMailSender extends Processor
{
    const SENDER_MAIL = 'noreply@cqr-assigment.nl';

    /** @var Mailer */
    private $mailer;

    /** @var AuthorRepository */
    private $authorRepository;

    public function __construct(Mailer $mailer, AuthorRepository $authorRepository)
    {
        $this->mailer = $mailer;
        $this->authorRepository = $authorRepository;
    }

    public function handleBlogPostWasCreated(BlogPostWasCreated $event, DomainMessage $domainMessage)
    {
        $authorId = $event->getAuthor();
        /** @var Author $author */
        $author = $this->authorRepository->find($authorId);
        $authorData = $author->serialize();

        $authorMail = $authorData['email'];
        $authorName = $authorData['first_name'];

        $body = sprintf('
        <p>Dear %s,</p>
        <p>You just made an post named %s</p>
        <p>This is an confirmation that your post has been processed successfully.</p>',
            $authorName,
            $event->getTitle()
        );
        $subject = 'Blog was created';

        mail($authorMail, $subject, $body);
    }
}