<?php

namespace Blog;

use Broadway\ReadModel\Repository;

interface PostRepository extends Repository
{
    public function findByPostId(int $postId): Post;

    /**
     * @return Post[]
     */
    public function findAll();

    /**
     * @return array
     */
    public function fetchAllIndexed(): array ;
}