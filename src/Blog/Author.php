<?php

namespace Blog;

interface Author
{
    public function toArray(): array ;

    public function getEmail(): string;

    public function getName(): string ;
}