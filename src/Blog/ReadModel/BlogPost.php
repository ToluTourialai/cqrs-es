<?php

namespace Blog\ReadModel;

use Broadway\Domain\DateTime;
use Broadway\ReadModel\SerializableReadModel;

class BlogPost implements SerializableReadModel
{
    /**
     * @var string
     */
    private $id, $title, $content, $author, $uuid;

    /**
     * @var DateTime
     */
    private $createdOn, $editedOn;

    public static function create(string $id, string $uuid, string $title, int $author, string $content, DateTime $createdOn): self
    {
        $post = new self();
        $post->id = $id;
        $post->uuid = $uuid;
        $post->title = $title;
        $post->content = $content;
        $post->author = $author;
        $post->createdOn = $createdOn;

        return $post;
    }

    public function edited(string $uuid, string $title, int $author, string $content, DateTime $editedOn)
    {
        $this->uuid = $uuid;
        $this->title = $title;
        $this->author = $author;
        $this->content = $content;
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return mixed The object instance
     */
    public static function deserialize(array $data)
    {
        $blogPost =  self::create(
            $data['id'],
            $data['uuid'],
            $data['title'],
            $data['author_id'],
            $data['content'],
            DateTime::fromString($data['created_on'])
        );

        if ($data['edited_on']) {
            $blogPost->editedOn = DateTime::fromString($data['edited_on']);
        }

        return $blogPost;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        $data = [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'title' => $this->title,
            'content' => $this->content,
            'author_id' => $this->author,
            'created_on' => $this->createdOn->toString(),
        ];

        if ($this->editedOn instanceof DateTime) {
            $data['edited_on'] = $this->editedOn->toString();
        }
        return $data;
    }
}