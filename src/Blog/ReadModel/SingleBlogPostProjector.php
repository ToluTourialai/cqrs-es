<?php

namespace Blog\ReadModel;

use Blog\Blog\Event\BlogPostWasCreated;
use Blog\Blog\Event\BlogPostWasEdited;
use Broadway\ReadModel\Repository;
use Broadway\Domain\DomainMessage;
use Broadway\ReadModel\Projector;
use Symfony\Component\Config\Definition\Exception\Exception;

class SingleBlogPostProjector extends Projector
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function applyBlogPostWasCreated(BlogPostWasCreated $event, DomainMessage $domainMessage)
    {
        $post = BlogPost::create(
            '0',
            $event->getId(),
            $event->getTitle(),
            $event->getAuthor(),
            $event->getContent(),
            $domainMessage->getRecordedOn()
        );

        $this->repository->save($post);
    }

    public function applyBlogPostWasEdited(BlogPostWasEdited $event, DomainMessage $domainMessage): void
    {
        /** @var BlogPost $blogPost */
        $blogPost = $this->repository->find($domainMessage->getId());

        $blogPost->edited(
            $domainMessage->getId(),
            $event->getTitle(),
            $event->getAuthor(),
            $event->getContent(),
            $domainMessage->getRecordedOn()
        );

        try {
            $this->repository->save($blogPost);
        } catch (\Exception $ex) {
            echo __FILE__.':'.__LINE__.'<pre>';
            var_dump($ex);
            die;
        }
    }
}