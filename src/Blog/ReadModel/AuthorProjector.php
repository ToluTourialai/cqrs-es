<?php

namespace Blog\ReadModel;

use Blog\Author\Event\AuthorWasCreated;
use Blog\Author\Event\AuthorWasEdited;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Repository;

class AuthorProjector extends Projector
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var string
     */
    private $id;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function applyAuthorWasCreated(AuthorWasCreated $event): void
    {
        $this->id = $event->getId();

        $author = Author::create(
            $event->getId(),
            $event->getAuthorId(),
            $event->getAuthorFirstName(),
            $event->getAuthorLasName(),
            $event->getAuthorMiddleName(),
            $event->getAuthorMail()
        );

        $this->repository->save($author);
    }

    public function applyAuthorWasEdited(AuthorWasEdited $event): void
    {
        $editAuthor = new Author();

        $editedAuthor = $editAuthor->edit(
            $this->id,
            $event->getAuthorFirstName(),
            $event->getAuthorLastName(),
            $event->getAuthorMiddleName(),
            $event->getAuthorMail()
        );

        $this->repository->save($editedAuthor);
    }
}