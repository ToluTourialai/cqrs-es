<?php

namespace Blog\ReadModel;

use Broadway\Domain\DateTime;
use Broadway\ReadModel\Identifiable;

class OverView implements Identifiable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var array
     */
    private $posts = [];

    public function addPost(string $id, string $title, string $author, string $content, DateTime $createdOn)
    {
        $post = [
            'id' => $id,
            'title' => $title,
            'author' => $author,
            'content' =>$content,
            'created' => $createdOn
        ];

        array_push($this->posts, $post);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return 1;
    }
}