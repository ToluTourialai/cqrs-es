<?php

namespace Blog\ReadModel;

use Broadway\ReadModel\Identifiable;
use Broadway\ReadModel\SerializableReadModel;

class Author implements Identifiable, SerializableReadModel
{
    /** @var string */
    private $id, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail;

    public static function create($id, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail)
    {
        $author = new self();
        $author->id = $id;
        $author->authorFirstName = $authorFirstName;
        $author->authorLastName = $authorLastName;
        $author->authorMiddleName = $authorMiddleName;
        $author->authorMail = $authorMail;

        return $author;
    }

    public function edit($id, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail)
    {
        $this->id = $id;
        $this->authorFirstName = $authorFirstName;
        $this->authorLastName = $authorLastName;
        $this->authorMiddleName = $authorMiddleName;
        $this->authorMail = $authorMail;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return mixed The object instance
     */
    public static function deserialize(array $data)
    {
        $id = $data['id'];
        $authorFirstName = $data['first_name'];
        $authorLastName = $data['last_name'];
        $authorMiddleName = $data['middle_name'];
        $authorMail = $data['email'];

        $author = self::create(
            $id,
            $authorFirstName,
            $authorLastName,
            $authorMiddleName,
            $authorMail
        );

        return $author;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'id' => $this->id,
            'first_name' => $this->authorFirstName,
            'last_name' => $this->authorLastName,
            'middle_name' => $this->authorMiddleName,
            'email' => $this->authorMail
        ];
    }
}