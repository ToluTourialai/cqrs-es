<?php

namespace Blog\ReadModel;

use Blog\Blog\Event\BlogPostWasCreated;
use Broadway\Domain\DomainMessage;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Repository;

class MultipleBlogPostProjector extends Projector
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var int
     */
    private $id;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
        $this->id = 1;
    }

    public function applyBlogPostWasCreated(BlogPostWasCreated $event, DomainMessage $domainMessage): void
    {
        $overview = $this->repository->find(1);

        if (!$overview){
            $overview = new OverView();
        }

        $overview->addPost(
            $event->getId(),
            $event->getTitle(),
            $event->getAuthor(),
            $event->getContent(),
            $domainMessage->getRecordedOn()
        );

        $this->repository->save($overview);
    }

    public function exists()
    {
        $this->repository->find($this->id);
    }
}