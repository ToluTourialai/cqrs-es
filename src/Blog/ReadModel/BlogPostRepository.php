<?php

namespace Blog\ReadModel;

use Broadway\ReadModel\Identifiable;
use Broadway\ReadModel\Repository;
use Broadway\Serializer\Serializer;
use Doctrine\DBAL\Connection;

class BlogPostRepository implements Repository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    private $tableName = 'Post';

    public function __construct(
        Connection $connection,
        Serializer $serializer
    ) {
        $this->connection = $connection;
        $this->serializer = $serializer;
    }

    /**
     * @param Identifiable $data
     */
    public function save(Identifiable $data)
    {
        $row = $this->convertToRow($data);

        /** @var BlogPost $data */
        $id = $data->getId();

        unset($row['id']);

        if (!$id) {
            $this->insert($row);

            return;
        }

        $this->update($id, $row);
    }

    /**
     * @param string $id
     * @param array  $row
     *
     * @return int
     */
    private function update($id, $row)
    {
        return $this->connection->update($this->tableName, $row, ['id' => $id]);
    }

    /**
     * @param string $id
     *
     * @return Identifiable|null
     */
    public function find($id)
    {
        return $this->fetchReadModel($id);
    }

    /**
     * @param array $fields
     *
     * @return Identifiable[]
     */
    public function findBy(array $fields)
    {
        return $this->fetchReadModelsBy($fields);
    }

    /**
     * @return Identifiable[]
     */
    public function findAll()
    {
        return $this->findBy([]);
    }

    /**
     * @param $id
     *
     * @return mixed|null
     */
    private function fetchReadModel($id)
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from($this->tableName)
            ->where('uuid = :id')
            ->setParameter('id', $id)
            ->execute();

        $row = $query->fetch(\PDO::FETCH_ASSOC);

        if ($row === false) {
            return;
        }

        return $this->serializer->deserialize([
            'class' => $row['class'],
            'payload' => $row,
        ]);
    }

    /**
     * @param array $fields
     *
     * @return array
     */
    private function fetchReadModelsBy(array $fields)
    {
        $query = $this->connection->createQueryBuilder()
            ->from($this->tableName)
            ->select('*');

        foreach ($fields as $field => $value) {
            $field = str_replace('.', '_', $field);

            $query->andWhere($field.' = :'.$field);
            $query->setParameter($field, $value);
        }

        $query = $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        $readmodels = [];

        foreach ($result as $row) {
            $readmodels[] = $this->serializer->deserialize([
                'class' => $row['class'],
                'payload' => $row,
            ]);
        }

        return $readmodels;
    }

    /**
     * @param Identifiable $data
     *
     * @return array
     */
    private function convertToRow(Identifiable $data)
    {
        $serialized = $this->serializer->serialize($data);

        $row = [];
        $row['class'] = get_class($data);

        $row = array_merge($row, $serialized['payload']);

        return $row;
    }

    private function insert($row)
    {
        $this->connection->insert($this->tableName, $row);
    }

    /**
     * @param string $id
     */
    public function remove($id)
    {
        throw new \RuntimeException('Incorrect action');
    }
}
