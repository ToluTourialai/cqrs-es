<?php

namespace Blog;

interface AuthorRepository
{
    public function findByAuthorById(int $id): Author;
}