<?php

namespace Blog\Author\Event;

class AuthorWasCreated
{
    /** @var string */
    private $id, $authorFirstName, $authorLasName, $authorMiddleName, $authorMail;

    /** @var int */
    private $authorId;

    /**
     * CreateAuthor constructor.
     * @param string $id
     * @param int $authorId
     * @param string $authorFirstName
     * @param string $authorLastName
     * @param string $authorMiddleName
     * @param string $authorMail
     */
    public function __construct(
        string $id,
        int $authorId,
        string $authorFirstName,
        string $authorLastName,
        string $authorMiddleName,
        string $authorMail
    ) {
        $this->id = $id;
        $this->authorId = $authorId;
        $this->authorFirstName = $authorFirstName;
        $this->authorLasName = $authorLastName;
        $this->authorMiddleName = $authorMiddleName;
        $this->authorMail = $authorMail;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthorFirstName(): string
    {
        return $this->authorFirstName;
    }

    /**
     * @return string
     */
    public function getAuthorLasName(): string
    {
        return $this->authorLasName;
    }

    /**
     * @return string
     */
    public function getAuthorMiddleName(): string
    {
        return $this->authorMiddleName;
    }

    /**
     * @return string
     */
    public function getAuthorMail(): string
    {
        return $this->authorMail;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }
}