<?php

namespace Blog\Author\Event;

class AuthorWasEdited
{
    /** @var string */
    private $authorFirstName, $authorLastName, $authorMiddleName, $authorMail;

    /**
     * Author constructor.
     * @param string $authorFirstName
     * @param string $authorLastName
     * @param string $authorMiddleName
     * @param string $authorMail
     */
    public function __construct(
        string $authorFirstName,
        string $authorLastName,
        string $authorMiddleName,
        string $authorMail
    )
    {
        $this->authorFirstName = $authorFirstName;
        $this->authorLastName = $authorLastName;
        $this->authorMiddleName = $authorMiddleName;
        $this->authorMail = $authorMail;
    }

    /**
     * @return string
     */
    public function getAuthorFirstName(): string
    {
        return $this->authorFirstName;
    }

    /**
     * @return string
     */
    public function getAuthorLastName(): string
    {
        return $this->authorLastName;
    }

    /**
     * @return string
     */
    public function getAuthorMiddleName(): string
    {
        return $this->authorMiddleName;
    }

    /**
     * @return string
     */
    public function getAuthorMail(): string
    {
        return $this->authorMail;
    }
}