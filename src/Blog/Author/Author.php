<?php

namespace Blog\Author;

use Blog\Author\Event\AuthorWasCreated;
use Blog\Author\Event\AuthorWasEdited;
use Broadway\EventSourcing\EventSourcedAggregateRoot;

class Author extends EventSourcedAggregateRoot
{
    /** @var string */
    private $id, $authorFirstName, $authorLasName, $authorMiddleName, $authorMail;

    /** @var int */
    private $authorId;

    public static function create(
        string $id,
        int $authorId,
        string $authorFirstName,
        string $authorLastName,
        string $authorMiddleName,
        string $authorMail
    )
    {
        $author = new self();

        $author->apply(new AuthorWasCreated(
            $id,
            $authorId,
            $authorFirstName,
            $authorLastName,
            $authorMiddleName,
            $authorMail
        ));

        return $author;
    }

    protected function applyAuthorWasCreated(AuthorWasCreated $event)
    {
        $this->id = $event->getId();
        $this->authorId = $event->getAuthorId();
        $this->authorFirstName = $event->getAuthorFirstName();
        $this->authorLasName = $event->getAuthorLasName();
        $this->authorMiddleName = $event->getAuthorMiddleName();
        $this->authorMail = $event->getAuthorMail();
    }

    public function edit(
        string $authorFirstNameEdit,
        string $authorLastNameEdit,
        string $authorMiddleName,
        string $authorMailEdit
    )
    {
        $this->apply(new AuthorWasEdited(
                $authorFirstNameEdit,
                $authorLastNameEdit,
                $authorMiddleName,
                $authorMailEdit
        ));
    }

    protected function applyAuthorWasEdited(AuthorWasEdited $event)
    {
        $this->authorFirstName = $event->getAuthorFirstName();
        $this->authorLasName = $event->getAuthorLastName();
        $this->authorMiddleName = $event->getAuthorMiddleName();
        $this->authorMail = $event->getAuthorMail();
    }

    /**
     * @return string
     */
    public function getAggregateRootId(): string
    {
        return $this->id;
    }
}