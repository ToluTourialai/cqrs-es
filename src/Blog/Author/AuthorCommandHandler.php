<?php

namespace Blog\Author;

use Blog\Author\Author;
use Blog\Author\Command\CreateAuthor;
use Blog\Author\Command\EditAuthor;
use Broadway\CommandHandling\SimpleCommandHandler;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\Repository\AggregateNotFoundException;
use Symfony\Component\Config\Definition\Exception\Exception;

class AuthorCommandHandler extends SimpleCommandHandler
{
    /**
     * @var EventSourcingRepository
     */
    private $repository;

    public function __construct(EventSourcingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handleCreateAuthor(CreateAuthor $command): void
    {
        $id = $command->getId();
        $authorId = $command->getAuthorId();
        $authorFirstName = $command->getAuthorFirstName();
        $authorLastName = $command->getAuthorLasName();
        $authorMiddleName = $command->getAuthorMiddleName();
        $authorMail = $command->getAuthorMail();

        $author = Author::create(
            $id,
            $authorId,
            $authorFirstName,
            $authorLastName,
            $authorMiddleName,
            $authorMail
        );

        $this->repository->save($author);
    }

    public function handleEditAuthor(EditAuthor $command): void
    {
        $id = $command->getId();
        if (!$this->exists($id)) {
            throw new Exception('Author does not exist');
        }

        $author = $this->getAuthor($id);

        $author->edit(
            $command->getAuthorFirstName(),
            $command->getAuthorLastName(),
            $command->getAuthorMiddleName(),
            $command->getAuthorMail()
        );

        $this->repository->save($author);
    }

    private function getAuthor($id): Author
    {
        /** @var Author $author */
        $author = $this->repository->load($id);

        return $author;
    }

    private function exists(string $id): bool
    {
        try {
            $this->getAuthor($id);

            return true;
        } catch (AggregateNotFoundException $ex) {
            return false;
        }
    }
}