<?php

namespace Blog\Author\Command;

use Broadway\Serializer\Serializable;

class EditAuthor implements Serializable
{
    /** @var string */
    private $id, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail;

    /**
     * Author constructor.
     * @param string $id
     * @param string $authorFirstName
     * @param string $authorLastName
     * @param string $authorMiddleName
     * @param string $authorMail
     */
    public function __construct(
        string $id,
        string $authorFirstName,
        string $authorLastName,
        string $authorMiddleName,
        string $authorMail
    )
    {
        $this->id = $id;
        $this->authorFirstName = $authorFirstName;
        $this->authorLastName = $authorLastName;
        $this->authorMiddleName = $authorMiddleName;
        $this->authorMail = $authorMail;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthorFirstName(): string
    {
        return $this->authorFirstName;
    }

    /**
     * @return string
     */
    public function getAuthorLastName(): string
    {
        return $this->authorLastName;
    }

    /**
     * @return string
     */
    public function getAuthorMiddleName(): string
    {
        return $this->authorMiddleName;
    }

    /**
     * @return string
     */
    public function getAuthorMail(): string
    {
        return $this->authorMail;
    }

    /**
     * @param array
     *
     * @return EditAuthor
     */
    public static function deserialize(array $data): self
    {
        $id = $data['id'];
        $authorFirstName = $data['authorFirstName'];
        $authorLastName = $data['authorLastName'];
        $authorMiddleName = $data['authorMiddleName'];
        $authorMail = $data['authorMail'];
        unset($data['id']);
        unset($data['authorFirstName']);
        unset($data['authorLastName']);
        unset($data['authorMiddleName']);
        unset($data['authorMail']);

        return new self($id, $authorFirstName, $authorLastName, $authorMiddleName, $authorMail);
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'authorFirstName' => $this->authorFirstName,
            'authorLastName' => $this->authorLastName,
            'authorMiddleName' => $this->authorMiddleName,
            'authorMail' => $this->authorMail
        ];
    }
}