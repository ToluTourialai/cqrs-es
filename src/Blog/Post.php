<?php

namespace Blog;

interface Post
{
    public function toArray(): array ;

    public function getPostId(): int;

    public function getOriginalAuthor(): int;
}