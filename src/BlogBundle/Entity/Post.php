<?php

namespace BlogBundle\Entity;

use Broadway\Domain\DateTime;
use FOS\OAuthServerBundle\Entity\Client;
use Blog\Post as BasePost;


class Post extends Client implements BasePost
{
    /** @var int */
    protected $id, $author_id, $edited_author_id;

    /** @var string */
    protected $title, $content, $uuid;

    /** @var DateTime */
    protected $createdOn, $editedOn;

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return int
     */
    public function getEditedAuthorId(): int
    {
        return $this->edited_author_id;
    }

    /**
     * @param int $edited_author_id
     */
    public function setEditedAuthorId(int $edited_author_id): void
    {
        $this->edited_author_id = $edited_author_id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->author_id;
    }

    /**
     * @param int $author_id
     */
    public function setAuthorId(int $author_id): void
    {
        $this->author_id = $author_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return DateTime
     */
    public function getCreatedOn(): DateTime
    {
        return $this->createdOn;
    }

    /**
     * @param DateTime $createdOn
     */
    public function setCreatedOn(DateTime $createdOn): void
    {
        $this->createdOn = $createdOn;
    }

    /**
     * @return DateTime
     */
    public function getEditedOn(): DateTime
    {
        return $this->editedOn;
    }

    /**
     * @param DateTime $editedOn
     */
    public function setEditedOn(DateTime $editedOn): void
    {
        $this->editedOn = $editedOn;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'author_id' => $this->author_id,
            'edited_author_id' => $this->edited_author_id,
            'title' => $this->title,
            'content' => $this->content,
            'created_on' => $this->createdOn,
            'edited_on' => $this->editedOn,
        ];
    }

    public function getPostId(): int
    {
        return $this->id;
    }

    public function getOriginalAuthor(): int
    {
        return $this->author_id;
    }
}