<?php

namespace BlogBundle\Repository;

use Blog\Post;
use Broadway\ReadModel\Identifiable;
use Doctrine\ORM\EntityRepository;
use Blog\PostRepository as BasePostRepository;

class PostRepository extends EntityRepository implements BasePostRepository
{

    public function findByPostId(int $postId): Post
    {
        return $this->find($postId);
    }

    /**
     * @return array
     */
    public function fetchAllIndexed(): array
    {
        return $this->findAll();
    }

    public function save(Identifiable $data)
    {
        // TODO: Implement save() method.
    }

    /**
     * @param string $id
     */
    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}