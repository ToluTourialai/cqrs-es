<?php

namespace BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Blog\Author;
use Blog\AuthorRepository as BaseAuthor;

class AuthorRepository extends EntityRepository implements BaseAuthor
{

    public function findByAuthorById(int $id): Author
    {
        return $this->findOneBy(['id' => $id]);
    }
}