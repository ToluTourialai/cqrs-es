<!DOCTYPE html>
<html>
<head>
    <title>Dit is een titel - CQRS</title>
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="http://materializecss.com/css/ghpages-materialize.css"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css"></head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="http://cqrs.dev.acpsit.nl/app_dev.php/" class="brand-logo center">Tolu CQRS</a>
        <ul class="left hide-on-med-and-down">
            <li class="nav-tabs"><a href="http://cqrs.dev.acpsit.nl/app_dev.php/">Overview</a></li>
            <li class="nav-tabs"><a href="http://cqrs.dev.acpsit.nl/app_dev.php/posts">Post</a></li>
            <li class="nav-tabs"><a href="http://cqrs.dev.acpsit.nl/app_dev.php/edit">Edit</a></li>
        </ul>
    </div>
</nav>
<nav>
    <div class="nav-wrapper">
        <div class="container">
        </div>
        <div class="row">
            <div class="col s12" style="background-color: #5c5757;">
                <a href="javascript:void" class="breadcrumb" style="font-size: 0.90em;">Home</a>
                <a href="javascript:void" class="breadcrumb" style="font-size: 0.90em;">Post</a>
                <a href="javascript:void" class="breadcrumb" style="font-size: 0.90em;">Dit is een titel</a>
            </div>
        </div>
    </div>
</nav>
<div class="row">
    <div class="col s12 m8">
        <div class="row">
            <div class="card">
                <div class="card-content black-text">
                    <span class="card-title black-text">Dit is een titel</span>
                    <p>Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content. Dit is aangepast content.</p>
                </div>
                <footer class="page-footer" style="margin:0px;padding:0px;background-color: #5c5757;">
                    <div class="footer-copyright">
                        <div class="container" style="width: 98%;">
                            <div class="col m12 s12">
                                <table>
                                    <tr>
                                        <td>Author : 2</td>
                                        <td>Modified : 12-01-2018 13:41:49</td>
                                        <td align="right" style="text-align: right"><b><a class="waves-effect waves-light btn pull-right" href="javascript:void()">Edit</a></b></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <div class="col s12 m4">
        <div class="card col s12 m12">
            <h4>Details</h4>
            <hr>
            <h5>Dit is een titel</h5>
            <ul class="collection">
                <li class="collection-item"><i class="tiny material-icons materialize-red-text">adjust</i> <span class="grey-text"> Name </span><span class="materialize-red-text">||</span> <span><i>2, 2</i></span></li>
                <li class="collection-item"><i class="tiny material-icons materialize-red-text">adjust</i> <span class="grey-text"> Modified </span><span class="materialize-red-text">||</span> <span><i>12-01-2018 13:41:49</i></span></li>
            </ul>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/js/materialize.min.js"></script>
</body>
</html>