<!DOCTYPE html>
<html>
<head>
    <title>[@blogTitle] - CQRS</title>
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="http://materializecss.com/css/ghpages-materialize.css"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css"></head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="http://cqrs.dev.acpsit.nl/app_dev.php/" class="brand-logo center">Tolu CQRS</a>
        <ul class="left hide-on-med-and-down">
            <li class="nav-tabs"><a href="http://cqrs.dev.acpsit.nl/app_dev.php/">Overview</a></li>
            <li class="nav-tabs"><a href="http://cqrs.dev.acpsit.nl/app_dev.php/posts">Post</a></li>
            <li class="nav-tabs"><a href="http://cqrs.dev.acpsit.nl/app_dev.php/edit">Edit</a></li>
        </ul>
    </div>
</nav>
<nav>
    <div class="nav-wrapper">
        <div class="container">
        </div>
        <div class="row">
            <div class="col s12" style="background-color: #5c5757;">
                <a href="javascript:void" class="breadcrumb" style="font-size: 0.90em;">Home</a>
                <a href="javascript:void" class="breadcrumb" style="font-size: 0.90em;">Post</a>
                <a href="javascript:void" class="breadcrumb" style="font-size: 0.90em;">[@blogTitle]</a>
            </div>
        </div>
    </div>
</nav>
<div class="row">
    <div class="col s12 m8">
        <div class="row">
            <div class="card">
                <div class="card-content black-text">
                    <span class="card-title black-text">[@blogTitle]</span>
                    <p>[@content]</p>
                </div>
                <footer class="page-footer" style="margin:0px;padding:0px;background-color: #5c5757;">
                    <div class="footer-copyright">
                        <div class="container" style="width: 98%;">
                            <div class="col m12 s12">
                                <table>
                                    <tr>
                                        <td>Author : [@authorName]</td>
                                        <td>[@type] : [@date]</td>
                                        <td align="right" style="text-align: right"><b><a class="waves-effect waves-light btn pull-right" href="javascript:void()">Edit</a></b></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <div class="col s12 m4">
        <div class="card col s12 m12">
            <h4>Details</h4>
            <hr>
            <h5>[@blogTitle]</h5>
            <ul class="collection">
                <li class="collection-item"><i class="tiny material-icons materialize-red-text">adjust</i> <span class="grey-text"> Name </span><span class="materialize-red-text">||</span> <span><i>[@authorName], [@authorName]</i></span></li>
                <li class="collection-item"><i class="tiny material-icons materialize-red-text">adjust</i> <span class="grey-text"> [@type] </span><span class="materialize-red-text">||</span> <span><i>[@date]</i></span></li>
            </ul>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/js/materialize.min.js"></script>
</body>
</html>