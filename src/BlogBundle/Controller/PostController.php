<?php

namespace BlogBundle\Controller;

use Blog\Blog\Command\CreateBlogPostCommand;
use Blog\Blog\Command\EditBlogPostCommand;
use Blog\ReadModel\BlogPost;
use Broadway\CommandHandling\CommandBus;
use Broadway\Domain\DateTime;
use Broadway\ReadModel\ElasticSearch\ElasticSearchRepository;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BlogBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('BlogBundle::default/index.html.twig', []);
    }

    /**
     * @Route("/posts", name="posts")
     */
    public function createPost(Request $request)
    {
        $form = $this->createBlogForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $request->request->get('form');

            $title = $post['Title'];
            $author = (int) $post['AuthorId'];
            $content = $post['Content'];
            $uuid = Uuid::uuid4();

            $this->getCommandBus()->dispatch(new CreateBlogPostCommand(
                Uuid::uuid4(),
                $uuid,
                $title,
                $author,
                $content
            ));

            return $this->redirectToRoute('homepage');
        }

        // replace this example code with whatever you need
        return $this->render('BlogBundle::default/posts.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="edit")
     */
    public function editPost(Request $request, $id)
    {
        $form = $this->createBlogForm();

        $form->handleRequest($request);

        $post = $this->getReadModel($id);
        $postValues = $post->serialize();

        if ($form->isSubmitted() && $form->isValid()) {
            $newPost = $request->request->get('form');

            $title = $newPost['Title'];
            $author = (int) $newPost['AuthorId'];
            $content = $newPost['Content'];

            $this->getCommandBus()->dispatch(new EditBlogPostCommand(
                $id,
                $title,
                $author,
                $content
            ));

            return $this->redirectToRoute('homepage');
        }

        // replace this example code with whatever you need
        return $this->render('BlogBundle::default/edit.html.twig', [
            'form' => $form->createView(),
            'title' => $postValues['title'],
            'content' => $postValues['content'],
            'author' => $postValues['author_id']
        ]);
    }

    /**
     * @Route("/view/{name}/", name="single")
     */
    public function getPost(Request $request, $name)
    {
        return $this->render('BlogBundle::renderedHTMLViews/'.$name.'.html.php');
    }

    public function createBlogForm()
    {
        $post = new Post();
        $post->setTitle('');
        $post->setContent('');
        $post->setAuthorId(0);
        $post->setCreatedOn(DateTime::now());

        $form = $this->createFormBuilder($post)
            ->add('Title', TextType::class, [
                'label' => 'Title',
                'attr' => [
                    'class' => 'validate'
                ],
                'required' => true,
            ])
            ->add('Content', TextareaType::class, [
                'label' => 'Content',
                'attr' => [
                    'class' => 'materialize-textarea'
                ],
                'required' => true,
            ])
            ->add('AuthorId', ChoiceType::class, [
                'choices' => [
                    'Tolu' => 1,
                    'Reen' => 2,
                    'Edwin' => 3,
                    'Derk' => 4,
                    'Rory' => 5,
                    'Eric' => 6,
                    'Milan' => 7,
                ],
                'label' => 'Author',
                'choice_attr' => [
                    'class' => 'materialize-textarea'
                ],
                'required' => true,
            ])
            ->add('Submit', SubmitType::class, [
                'label' => 'Submit',
                'attr' => [
                    'class' => 'waves-effect waves-light btn'
                ]
            ])
            ->getForm();

        return $form;
    }

    /**
     * @return CommandBus
     */
    public function getCommandBus()
    {
        $commandBus = $this->get('broadway.command_handling.command_bus');

        return $commandBus;
    }

    private function getReadModel(string $id): BlogPost
    {
        /** @var ElasticSearchRepository $repository */
        $repository = $this->get('blog.blog.read_model_repository');

        return $repository->find($id);
    }
}